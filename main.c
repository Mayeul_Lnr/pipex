/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/01 14:09:05 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/07/20 23:45:42 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

int	pipex_get_paths(char ***av, char **cmd, char **envp, char ***paths)
{
	int	i;

	if (!ft_split_n3(*cmd, ' ', av))
		return (-1);
	*cmd = NULL;
	if (!**av)
		return (2);
	i = 0;
	while (envp[i] && envp[i] != ft_strnstr(envp[i], "PATH=", 5))
		i++;
	if ((*av)[0][0] == '/' || ((*av)[0][0] == '.' && (*av)[0][1] == '/'))
	{
		if (!ft_strdup_n3((*av)[0], cmd))
			return (split_free(av) - 1);
		return (1);
	}
	if (!envp[i])
		return (2);
	if (!ft_split_n3(envp[i] + 5, ':', paths))
		return (split_free(av) - 1);
	return (0);
}

int	pipex_parse(char ***av, char **cmd, char **envp)
{
	int		i;
	int		j;
	char	**paths;

	j = pipex_get_paths(av, cmd, envp, &paths);
	if (j)
		return (j);
	i = 0;
	j = -1;
	while (paths[i] && j)
	{
		j = ft_strlen(paths[i]);
		if (paths[i][j - 1] != '/')
			paths[i][j] = '/';
		if (!ft_cat_n3(paths[i], (*av)[0], cmd))
			return (split_free(&paths) + split_free(av) - 1);
		j = access(*cmd, F_OK);
		if (j)
			free(*cmd);
		i++;
	}
	split_free(&paths);
	if (j && !ft_strdup_n3((*av)[0], cmd))
		return (split_free(av) + ft_free(*cmd) - 1);
	return (-2 * j);
}

int	child_process(int ac, char **av, char **envp, t_pipex_data *dat)
{
	if (dat->k == ac - 4)
	{
		dat->out[0] = -1;
		dat->out[1] = open(av[ac - 1], O_CREAT + O_WRONLY
				+ !dat->here_doc * O_TRUNC + dat->here_doc * O_APPEND, 0664);
		if (dat->out[1] < 0)
			pipex_err(av[0], av[ac - 1]);
	}
	close(dat->out[0]);
	close(dat->in[1]);
	dat->cmd = av[dat->k + 2];
	dat->cnf = pipex_parse(&dat->av, &dat->cmd, envp);
	if (dat->in[0] < 0 || dat->out[1] < 0 || dat->cnf < 0)
		exit(EXIT_FAILURE);
	if (dat->cnf < 2 && dup2(dat->in[0], 0) >= 0 && dup2(dat->out[1], 1) >= 0)
		execve(dat->cmd, dat->av, envp);
	if (dat->cnf < 2)
		pipex_err(av[0], dat->cmd);
	else
		cnf_err(av[0], dat->cmd);
	dat->cnf = dat->cnf < 2 && !access(dat->cmd, F_OK);
	ft_free(dat->cmd);
	split_free(&dat->av);
	exit(127 - dat->cnf);
}

int	child_generator(int ac, t_pipex_data *dat)
{
	while (dat->pid > 0 && dat->k < ac - 3)
	{
		if (dat->k != ac - 4)
			if (pipe(dat->out))
				exit(EXIT_FAILURE);
		dat->pid = fork();
		if (dat->pid < 0)
			exit(EXIT_FAILURE);
		else if (dat->pid > 0)
		{
			close(dat->in[0]);
			close(dat->in[1]);
			dat->in[0] = dat->out[0];
			dat->in[1] = dat->out[1];
			dat->k++;
		}
	}
	return (0);
}

int	main(int ac, char **av, char **envp)
{
	t_pipex_data	dat;

	if (ac != 5)
		return (write(2, "usage: ./pipex file1 cmd1 cmd2 file2\n", 37));
	dat.in[0] = open(av[1], O_RDONLY);
	if (dat.in[0] < 0)
	{
		pipex_err(av[0], av[1]);
		dat.k = 0;
	}
	dat.in[1] = -1;
	dat.pid = 1;
	child_generator(ac, &dat);
	if (!dat.pid)
		child_process(ac, av, envp, &dat);
	waitpid(dat.pid, &dat.status, 0);
	return (WEXITSTATUS(dat.status));
}
