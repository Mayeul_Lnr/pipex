/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc_bonus.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/16 17:56:40 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/07/18 23:30:16 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./pipex.h"

int	heredoc_loop(int fd, const char *delimiter)
{
	char	*s;
	int		nb;
	int		len;

	nb = 1;
	len = ft_strlen(delimiter);
	while (nb > 0)
	{
		write(1, "> ", 2);
		nb = get_next_line(0, &s);
		if (nb > 0 && ft_strncmp(s, delimiter, len + 1))
		{
			write(fd, s, ft_strlen(s));
			write(fd, "\n", 1);
		}
		else
			nb = 0;
		free(s);
		s = NULL;
	}
	return (0);
}

int	heredoc(const char *delimiter)
{
	int		fd[2];

	if (pipe(fd) < 0)
		return (-1);
	heredoc_loop(fd[1], delimiter);
	close(fd[1]);
	return (fd[0]);
}
