MANDA_SRCS 	=	main.c

BONUS_SRCS	=	main_bonus.c \
				gnl_bonus.c \
				heredoc_bonus.c

COMMON_SRCS	=	ft_utils.c \
				ft_utils2.c \
				ft_split.c

OBJS		= ${COMMON_SRCS:.c=.o} ${MANDA_SRCS:.c=.o}
BONUS_OBJS	= ${COMMON_SRCS:.c=.o} ${BONUS_SRCS:.c=.o}

NAME		= pipex
CC			= clang -Wall -Wextra -Werror
RM			= rm -f

${NAME}:	${OBJS}
	${CC} ${OBJS} -o ${NAME}

all:		${NAME}

bonus:		${BONUS_OBJS}
	${CC} ${BONUS_OBJS} -o ${NAME}

%.o:		%.c
	${CC} -c $< -o $@

clean:
	${RM} ${OBJS} ${BONUS_OBJS}

fclean:		clean
	${RM} ${NAME}

re:			fclean all

.PHONY:		all bonus clean fclean re
