/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/01 14:19:21 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/07/19 04:10:45 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_H
# define PIPEX_H

# include <unistd.h>
# include <stdlib.h>
# include <errno.h>
# include <string.h>
# include <fcntl.h>
# include <sys/wait.h>

# define GNL_BUFFER_SIZE 64

typedef struct s_pipex_data
{
	int		in[2];
	int		out[2];
	int		pid;
	char	*cmd;
	char	**av;
	int		cnf;
	int		k;
	int		status;
	int		here_doc;
}	t_pipex_data;

char	*ft_strdup_n3(const char *s1, char **ret);

char	*ft_cat_n3(const char *s1, const char *s2, char **ret);

char	*ft_strnstr(const char *s1, const char *s2, size_t n);

int		ft_strlen(const char *s);

char	**ft_split_n3(const char *s, char c, char ***ret);

int		ft_free(void *ptr);

int		split_free(char ***ptr);

int		pipex_err(char *s1, char *s2);

int		cnf_err(char *s1, char *s2);

int		ft_strncmp(const char *s1, const char *s2, size_t n);

/*
**	Here are somes bonus functions that should never be called
**	in the mandatory part
*/

int		get_next_line(int fd, char **line);

int		heredoc(const char *delimiter);

#endif
