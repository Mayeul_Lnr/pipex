/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/01 14:40:19 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/07/18 19:47:45 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

char	*ft_strdup_n3(const char *s1, char **ret)
{
	size_t	len;
	size_t	i;

	len = 0;
	while (s1[len])
		len++;
	*ret = malloc(len + 1);
	if (!*ret)
		return (NULL);
	i = 0;
	while (i < len)
	{
		(*ret)[i] = s1[i];
		i++;
	}
	(*ret)[i] = 0;
	return (*ret);
}

char	*ft_cat_n3(const char *s1, const char *s2, char **ret)
{
	int		i;
	int		j;

	i = 0;
	while (s1[i])
		i++;
	j = 0;
	while (s2[j])
		j++;
	*ret = malloc(i + j + 1);
	if (!*ret)
		return (NULL);
	i = 0;
	j = 0;
	while (s1[i])
		(*ret)[j++] = s1[i++];
	i = 0;
	while (s2[i])
		(*ret)[j++] = s2[i++];
	(*ret)[j] = 0;
	return (*ret);
}

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t	i;

	i = 0;
	if (s1 == s2)
		return (0);
	if (!s1 || !s2)
		return (1);
	while (i < n && !(!s1[i] && !s2[i]))
	{
		if (s1[i] != s2[i])
			return ((unsigned char)s1[i] - (unsigned char)s2[i]);
		i++;
	}
	return (0);
}

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t	i;
	size_t	len;

	if (!*s2)
		return ((char *)s1);
	i = 0;
	len = 0;
	while (s2[len])
		len++;
	while (s1[i] && i + len - 1 < n)
	{
		if (!ft_strncmp(s1 + i, s2, len))
			return ((char *)s1 + i);
		i++;
	}
	return (NULL);
}

int	ft_strlen(const char *s)
{
	int	ret;

	if (!s)
		return (0);
	ret = 0;
	while (s[ret])
		ret++;
	return (ret);
}
