/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/02 17:38:30 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/07/18 21:48:47 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

char	*ft_memcpy(char *s1, const char *s2, size_t n)
{
	size_t	i;

	if (!s1 && !s2)
		return (NULL);
	i = 0;
	while (i < n)
	{
		s1[i] = s2[i];
		i++;
	}
	return (s1 + i);
}

int	cnf_err(char *s1, char *s2)
{
	int		len1;
	int		len2;
	char	*s;

	len1 = ft_strlen(s1);
	len2 = ft_strlen(s2);
	s = malloc(len1 + len2 + 23);
	if (!s)
		return (-1);
	ft_memcpy(ft_memcpy(ft_memcpy(s, s1, len1), ": ", 2), s2, len2);
	ft_memcpy(s + len1 + len2 + 2, ": command not found\n", 20);
	write(2, s, len1 + len2 + 22);
	free(s);
	return (0);
}

int	pipex_err(char *s1, char *s2)
{
	int		len[3];
	char	*s3;
	char	*s;

	s3 = strerror(errno);
	len[0] = ft_strlen(s1);
	len[1] = ft_strlen(s2);
	len[2] = ft_strlen(s3);
	s = malloc(len[0] + len[1] + len[2] + 5);
	if (!s)
		return (-1);
	ft_memcpy(s, s1, len[0]);
	if (s1)
		ft_memcpy(s + len[0], ": ", 2);
	s1 = ft_memcpy(s + len[0] + 2 * (s1 != NULL), s2, len[1]);
	if (s2)
		s1 = ft_memcpy(s1, ": ", 2);
	s1 = ft_memcpy(s1, s3, len[2]);
	*s1 = '\n';
	write(2, s, s1 - s + 1);
	free(s);
	return (0);
}

int	ft_free(void *ptr)
{
	free(ptr);
	return (0);
}

int	split_free(char ***ptr)
{
	int	i;

	if (*ptr)
	{
		i = 0;
		while ((*ptr)[i])
		{
			free((*ptr)[i]);
			i++;
		}
		free(*ptr);
		*ptr = NULL;
	}
	return (0);
}
