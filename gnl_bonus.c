/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gnl_bonus.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/23 15:56:48 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/07/16 19:15:20 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./pipex.h"

int	buffer_len(char *s)
{
	size_t	len;

	if (!s)
		return (0);
	len = 0;
	while (s[len] && s[len] != '\n')
		len++;
	return (len);
}

char	*buffer_join(char *buffer, char *line)
{
	size_t	i;
	size_t	j;
	char	*ret;

	i = buffer_len(buffer) + buffer_len(line);
	ret = malloc(i + 1);
	if (!ret)
		return (NULL);
	i = 0;
	while (line[i])
	{
		ret[i] = line[i];
		i++;
	}
	j = 0;
	while (buffer[j] && buffer[j] != '\n')
	{
		ret[i] = buffer[j];
		i++;
		j++;
	}
	ret[i] = '\0';
	return (ret);
}

int	flush_buffer(char *buffer, char **line)
{
	size_t	i;
	size_t	j;
	int		ret;
	char	*temp;

	temp = buffer_join(buffer, *line);
	if (!temp)
	{
		free(*line);
		*line = NULL;
		return (0);
	}
	free(*line);
	*line = temp;
	j = 0;
	while (buffer[j] && buffer[j] != '\n')
		j++;
	ret = (buffer[j] == '\n');
	i = 0;
	while (buffer[j])
		buffer[i++] = buffer[++j];
	return (ret);
}

static int	panic_free(char **s)
{
	free(*s);
	*s = NULL;
	return (-1);
}

int	get_next_line(int fd, char **line)
{
	static char	buff[GNL_BUFFER_SIZE + 2] = {0};
	int			n;

	if (!line)
		return (-1);
	*line = malloc(1);
	if (!*line)
		return (-2);
	**line = '\0';
	if (fd < 0 || GNL_BUFFER_SIZE < 1)
		return (panic_free(line));
	n = 1;
	while (!flush_buffer(buff, line) && n)
	{
		if (!*line)
			return (panic_free(line));
		n = read(fd, buff, GNL_BUFFER_SIZE);
		if (n < 0)
			return (panic_free(line));
		buff[n] = '\0';
	}
	return ((n > 0) - (n == -1));
}
